package io.dublinbrains.videobay.model.advert;

import java.util.Arrays;

public enum AdvertState {

	OPEN("open"), PENDING("pending"), CLOSED("closed"), CANCELD("canceld"), SOLD("sold");

	private String value;

	private AdvertState(String value) {
		this.value = value;
	}

	public static AdvertState fromValue(String value) throws IllegalArgumentException {
		for (AdvertState category : values()) {
			if (category.value.equalsIgnoreCase(value)) {
				return category;
			}
		}
		throw new IllegalArgumentException("Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
	}

}
