package io.dublinbrains.videobay.model.advert;

import java.util.Arrays;

public enum Country {

	IRL("IRL");
	
	private final String isoCode;
	
	private Country(String isoCode) {
		this.isoCode = isoCode;
		
	}

	public static Country fromString(String text) throws IllegalArgumentException {
		if (text != null) {
			for (Country c : Country.values()) {
				if (text.equalsIgnoreCase(c.isoCode)) {
					return c;
				}
			}
		}
		throw new IllegalArgumentException("Unknown enum type " + text + ", Allowed values are " + Arrays.toString(values()));
	}

}
