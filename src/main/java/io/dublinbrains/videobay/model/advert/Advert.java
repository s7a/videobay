package io.dublinbrains.videobay.model.advert;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import io.dublinbrains.videobay.model.conversation.Conversation;
import io.dublinbrains.videobay.model.user.BayUser;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonPropertyOrder({ "id", "userId", "title", "description", "price", "state", "country", "videoUrl" })
@Entity
public class Advert
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String title;
	private String description;
	private BigDecimal price;
	private Long created;
	private String county;
	private AdvertState state;
	private Country country;
	private String videoUrl;
	private String city;
	private String category;
	@Transient
	private Long userId;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	// Maybe this is a good idea since the user with all other adverts may be
	// useless for the UI
	@JsonIgnore
	private BayUser user;
	
	@OneToMany(mappedBy = "advert", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Conversation> conversations;

	public Advert()
	{
	};

	

	public Advert(String title, String description, BigDecimal price, Long created, String county, AdvertState state,
			Country country, String videoUrl, String city, String category, Long userId, BayUser user,
			List<Conversation> conversations) {
		super();
		this.title = title;
		this.description = description;
		this.price = price;
		this.created = created;
		this.county = county;
		this.state = state;
		this.country = country;
		this.videoUrl = videoUrl;
		this.city = city;
		this.category = category;
		this.userId = userId;
		this.user = user;
		this.conversations = conversations;
	}



	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public BigDecimal getPrice()
	{
		return price;
	}

	public void setPrice(BigDecimal price)
	{
		this.price = price;
	}

	public String getVideoUrl()
	{
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl)
	{
		this.videoUrl = videoUrl;
	}

	public Long getId()
	{
		return id;
	}

	public AdvertState getState()
	{
		return state;
	}

	public void setState(AdvertState state)
	{
		this.state = state;
	}

	public Country getCountry()
	{
		return country;
	}

	public void setCountry(Country country)
	{
		this.country = country;
	}

	public Long getCreated()
	{
		return created;
	}

	public void setCreated(Long created)
	{
		this.created = created;
	}

	public String getCounty()
	{
		return county;
	}

	public void setCounty(String county)
	{
		this.county = county;
	}

	public BayUser getUser()
	{
		return user;
	}

	public void setUser(BayUser user)
	{
		this.user = user;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public Long getUserId()
	{
		return this.user.getId();
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public String getCategory()
	{
		return this.category;
	}

	public final List<Conversation> getConversations() {
		return conversations;
	}

	public final void setConversations(List<Conversation> conversations) {
		this.conversations = conversations;
	}
	
	

}
