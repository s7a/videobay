package io.dublinbrains.videobay.model.advert;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdvertRepository extends JpaRepository<Advert, Long>
{

	public Advert findByDescription(String description);

	@Query(value = "SELECT * FROM advert WHERE user_id = :userId", nativeQuery = true)
	public List<Advert> findByUserId(@Param("userId") Long userId);

	@Query(value = "SELECT * FROM advert WHERE id IN :ids", nativeQuery = true)
	public List<Advert> findByRangeOfIds(@Param("ids") Collection<Long> ids);

	public List<Advert> findByCountryAndState(Country country, AdvertState state);

	@Query(value = "SELECT * FROM advert WHERE  user_id = :userId AND state = :state", nativeQuery = true)
	public List<Advert> findByUserIdAndState(@Param("userId") Long userId, @Param("state") AdvertState state);

}
