package io.dublinbrains.videobay.model.subscription;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long>{

	public Subscription findById(Long id);

    public Subscription findByEmail(String email);
}
