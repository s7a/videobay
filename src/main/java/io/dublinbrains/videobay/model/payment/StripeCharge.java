package io.dublinbrains.videobay.model.payment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class StripeCharge {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String stripeUserId;
	
	private String stripeCreditCardToken;
	
	private String creditCardSuffix;
	
	
	public StripeCharge(){};
	
	public StripeCharge(String stripeUserId) {
		super();
		this.setStripeUserId(stripeUserId);
	}

	public String getStripeUserId() {
		return stripeUserId;
	}

	public void setStripeUserId(String stripeUserId) {
		this.stripeUserId = stripeUserId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStripeCreditCardToken() {
		return stripeCreditCardToken;
	}

	public void setStripeCreditCardToken(String stripeCreditCardToken) {
		this.stripeCreditCardToken = stripeCreditCardToken;
	}

	public String getCreditCardSuffix() {
		return creditCardSuffix;
	}

	public void setCreditCardSuffix(String creditCardSuffix) {
		this.creditCardSuffix = creditCardSuffix;
	}
}
