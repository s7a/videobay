package io.dublinbrains.videobay.model.payment;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.advert.Advert;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
public class Payment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Advert advert;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Account accountSeller;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Account accountBuyer;
	
	@OneToOne(cascade = CascadeType.ALL)
	private StripeCharge stripeCharge;
	
	public Payment(){};
	
	public Payment(Account accountSeller, Account accountBuyer, Advert advert, StripeCharge stripeCharge) {
		super();
		this.setAccountSeller(accountSeller);
		this.setAccountBuyer(accountBuyer);
		this.advert = advert;
		this.setStripeCharge(stripeCharge);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Advert getAdvert() {
		return advert;
	}

	public void setAdvert(Advert advert) {
		this.advert = advert;
	}

	public Account getAccountSeller() {
		return accountSeller;
	}

	public void setAccountSeller(Account accountSeller) {
		this.accountSeller = accountSeller;
	}

	public Account getAccountBuyer() {
		return accountBuyer;
	}

	public void setAccountBuyer(Account accountBuyer) {
		this.accountBuyer = accountBuyer;
	}

	public StripeCharge getStripeCharge() {
		return stripeCharge;
	}

	public void setStripeCharge(StripeCharge stripeCharge) {
		this.stripeCharge = stripeCharge;
	}
}
