package io.dublinbrains.videobay.model.account;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.dublinbrains.videobay.model.payment.StripeCharge;
import io.dublinbrains.videobay.model.user.BayUser;

@Entity
public class Account
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private List<Address> addresses;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = true)
	@JsonIgnore
	BayUser user;

	@OneToMany(cascade = CascadeType.ALL)
	private List<StripeCharge> stripeCharges;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "paymentAccountId")
	private PaymentAccount paymentAccount;

	public Account()
	{
	};

	public Account(Long id, List<Address> address, BayUser user)
	{
		super();
		this.id = id;
		this.addresses = address;
		this.user = user;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public final List<Address> getAddresses()
	{
		return addresses;
	}

	public final void setAddresses(List<Address> addresss)
	{
		this.addresses = addresss;
	}

	public final BayUser getUser()
	{
		return user;
	}

	public final void setUser(BayUser user)
	{
		this.user = user;
	}

	public List<StripeCharge> getStripeCharges()
	{
		return stripeCharges;
	}

	public void setStripeCharges(List<StripeCharge> stripeCharges)
	{
		this.stripeCharges = stripeCharges;
	}

	public PaymentAccount getPaymentAccount()
	{
		return this.paymentAccount;
	}

	public void setPaymentAccount(PaymentAccount paymentAccount)
	{
		this.paymentAccount = paymentAccount;
	}
}
