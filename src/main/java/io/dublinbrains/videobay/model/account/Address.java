package io.dublinbrains.videobay.model.account;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.dublinbrains.videobay.model.advert.Country;

@Entity
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Country country;
	
	private String fullName;
	
	private String streetAddress_1;
	
	private String streetAddress_2;
	
	private String city;
	
	private String county;
	
	private String postCode;
	
	private String phoneNumber;
	
	private Boolean isPrimary;
	
	@ManyToOne
	@JoinColumn(name = "account_id", nullable = false)
	@JsonIgnore
	private Account account;
	
	public Address(){};
	
	public Address(Long id, Country country, String fullName, String streetAddress_1, String streetAddress_2,
			String city, String county, String postCode, String phoneNumber, Boolean isPrimary, Account account) {
		super();
		this.id = id;
		this.country = country;
		this.fullName = fullName;
		this.streetAddress_1 = streetAddress_1;
		this.streetAddress_2 = streetAddress_2;
		this.city = city;
		this.county = county;
		this.postCode = postCode;
		this.phoneNumber = phoneNumber;
		this.isPrimary = isPrimary;
		this.account = account;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public final Country getCountry() {
		return country;
	}

	public final void setCountry(Country country) {
		this.country = country;
	}

	public final String getFullName() {
		return fullName;
	}

	public final void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public final String getStreetAddress_1() {
		return streetAddress_1;
	}

	public final void setStreetAddress_1(String streetAddress_1) {
		this.streetAddress_1 = streetAddress_1;
	}

	public final String getStreetAddress_2() {
		return streetAddress_2;
	}

	public final void setStreetAddress_2(String streetAddress_2) {
		this.streetAddress_2 = streetAddress_2;
	}

	public final String getCity() {
		return city;
	}

	public final void setCity(String city) {
		this.city = city;
	}

	public final String getCounty() {
		return county;
	}

	public final void setCounty(String county) {
		this.county = county;
	}

	public final String getPostCode() {
		return postCode;
	}

	public final void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public final String getPhoneNumber() {
		return phoneNumber;
	}

	public final void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public final Boolean getIsPrimary() {
		return isPrimary;
	}

	public final void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public final Account getAccount() {
		return account;
	}

	public final void setAccount(Account account) {
		this.account = account;
	}
}
