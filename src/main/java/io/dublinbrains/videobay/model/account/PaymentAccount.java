package io.dublinbrains.videobay.model.account;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PaymentAccount
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String bankName;

	private String recepientName;

	private String iban;

	private String bic;

	@OneToOne(mappedBy = "paymentAccount", cascade = CascadeType.ALL, optional = true)
	@JsonIgnore
	private Account account;

	public PaymentAccount()
	{
	};

	public PaymentAccount(Long id, String bankName, String recepientName, String iban, String bic)
	{
		super();
		this.setId(id);
		this.setBankName(bankName);
		this.setRecepientName(recepientName);
		this.setIban(iban);
		this.setBic(bic);
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public String getRecepientName()
	{
		return recepientName;
	}

	public void setRecepientName(String recepientName)
	{
		this.recepientName = recepientName;
	}

	public String getIban()
	{
		return iban;
	}

	public void setIban(String iban)
	{
		this.iban = iban;
	}

	public String getBic()
	{
		return bic;
	}

	public void setBic(String bic)
	{
		this.bic = bic;
	}
}
