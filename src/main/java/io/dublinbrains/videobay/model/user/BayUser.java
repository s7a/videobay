package io.dublinbrains.videobay.model.user;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.advert.Advert;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonPropertyOrder({ "id", "firstName", "surName", "screenName", "email", "password" })
@Entity
public class BayUser
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String firstName;

	private String surName;

	private String screenName;

	private String password;

	private String email;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Advert> adverts;

	@ElementCollection
	// @CollectionTable(name = "FAVORITE_ADS")
	private Set<Long> favoriteAds;

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, optional = true)
	private Account account;

	public BayUser()
	{
	};

	public BayUser(Long id, String firstName, String surName, String screenName, String password, String email,
			List<Advert> adverts, Set<Long> favoriteAds, Account account)
	{
		super();
		this.id = id;
		this.firstName = firstName;
		this.surName = surName;
		this.screenName = screenName;
		this.password = password;
		this.email = email;
		this.adverts = adverts;
		this.favoriteAds = favoriteAds;
		this.account = account;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getSurName()
	{
		return surName;
	}

	public void setSurName(String surName)
	{
		this.surName = surName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getScreenName()
	{
		return screenName;
	}

	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public List<Advert> getAdverts()
	{
		return adverts;
	}

	public void setAdverts(List<Advert> adverts)
	{
		this.adverts = adverts;
	}

	public Set<Long> getFavoriteAds()
	{
		return favoriteAds;
	}

	public void setFavoriteAds(Set<Long> favorites)
	{
		this.favoriteAds = favorites;
	}

	public final Account getAccount()
	{
		return account;
	}

	public final void setAccount(Account account)
	{
		this.account = account;
	}
}
