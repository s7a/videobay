package io.dublinbrains.videobay.model.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<BayUser, Long>{

	public BayUser findById(Long id);

    public BayUser findByEmail(String email);

	public BayUser findByScreenName(String screenName);
	
	public BayUser findByScreenNameAndPassword(String screenName, String password);

}
