package io.dublinbrains.videobay.model.conversation;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import io.dublinbrains.videobay.model.advert.Advert;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonPropertyOrder({ "id", "questionerId", "question", "answer", "created"})
@Entity
public class Conversation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long questionerId;
	private String question;
	private String answer;
	private Long created;
	
	@ManyToOne
	@JoinColumn(name = "advert_id", nullable = false)
	@JsonIgnore
	private Advert advert;
	
	public Conversation(){};

	

	public Conversation(Long questionerId, String question, String answer, Long created, Advert advert) {
		super();
		this.questionerId = questionerId;
		this.question = question;
		this.answer = answer;
		this.created = created;
		this.advert = advert;
	}



	public final Long getQuestionerId() {
		return questionerId;
	}

	public final void setQuestionerId(Long questionerId) {
		this.questionerId = questionerId;
	}

	public final String getQuestion() {
		return question;
	}

	public final void setQuestion(String question) {
		this.question = question;
	}

	public final String getAnswer() {
		return answer;
	}

	public final void setAnswer(String answer) {
		this.answer = answer;
	}

	public final Advert getAdvert() {
		return advert;
	}

	public final void setAdvert(Advert advert) {
		this.advert = advert;
	}
	
	public final Long getId(){
		return id;
	}

	public final Long getCreated() {
		return created;
	}

	public final void setCreated(Long created) {
		this.created = created;
	}
	
	

}
