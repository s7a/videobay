package io.dublinbrains.videobay.model.conversation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface ConversationRepository extends JpaRepository<Conversation, Long>{
	
	@Query(value = "SELECT * FROM Conversation c WHERE c.advert_id = :advertId", nativeQuery = true)
	public List<Conversation> findByAdvertId(@Param("advertId") Long advertId);

}
