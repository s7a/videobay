package io.dublinbrains.videobay.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Main VideoBay spring boot bootstrap
 *
 */
@SpringBootApplication
@ComponentScan("io.dublinbrains.videobay.*")
public class VideoBayApp 
{
    public static void main( String[] args )	
    {
    	
    	SpringApplication.run(VideoBayApp.class, args);
    }
}
