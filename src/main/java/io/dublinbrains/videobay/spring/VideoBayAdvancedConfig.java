package io.dublinbrains.videobay.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
@Order(2)
public class VideoBayAdvancedConfig implements InfoContributor, EmbeddedServletContainerCustomizer {

	@Value("${server.port}")
	private String port;

	@Value("${contect.path}")
	private String contextPath;

	@Value("${app.name}")
	private String appName;

	@Value("${info.app.version}")
	private String appVersion;

	// @Value("${info.app.commitid}")
	// private String commitId;

	@Value("${info.app.name}")
	private String springName;

	@Override
	public void contribute(Info.Builder builder) {

		// This is stringe @ Citi the properties spring config are working !!!!
		// Map<String, String> details = new HashMap<>();
		// details.put("AppName", appName);
		// details.put("Version", appVersion);
		// // details.put("Commit_ID", commitId);
		// details.put("SpringName", springName);
		// builder.withDetail(appName,Collections.singletonMap("key", appVersion));

	}

	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		container.setContextPath(contextPath);
		container.setDisplayName(appName);

	}

	@Bean
	public CommonsRequestLoggingFilter logFilter() {
		CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(10000);
		filter.setIncludeHeaders(false);
		filter.setAfterMessagePrefix("REQUEST DATA : ");
		return filter;
	}

}
