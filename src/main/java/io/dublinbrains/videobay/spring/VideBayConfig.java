package io.dublinbrains.videobay.spring;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;


@SpringBootConfiguration
@EntityScan(basePackages = {"io.dublinbrains.videobay.model"})
@Order(1)
public class VideBayConfig {

	@Value("${server.port}")
	private String port;
	
	   @Bean
	    public PropertyPlaceholderConfigurer getPropertyPlaceholderConfigurer()
	            throws IOException {
	        PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
	        ppc.setLocations(new PathMatchingResourcePatternResolver().getResources("classpath:*.properties"));
	        return ppc;
	    }

	
	

}
