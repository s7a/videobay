package io.dublinbrains.videobay.service.user;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.dublinbrains.videobay.exceptions.EntityExistsException;
import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.account.AccountRepository;
import io.dublinbrains.videobay.model.account.Address;
import io.dublinbrains.videobay.model.account.PaymentAccount;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.model.user.UserRepository;

@Service
public class UserService
{

	Logger logger = LogManager.getLogger(UserService.class.getName());

	@Autowired
	UserRepository userRepo;

	@Autowired
	AccountRepository accountRepo;

	public Account saveBankingDetails(Long accountId, PaymentAccount details) throws Exception
	{
		if (accountId != null && details != null)
		{
			final Account account = accountRepo.findOne(accountId);

			if (account != null)
			{
				account.setPaymentAccount(details);
				accountRepo.save(account);
				return account;
			}
			else
			{
				logger.warn("No account found for accountId: " + accountId);
				throw new Exception("No account found for accountId: " + accountId);
			}
		}
		logger.warn("Invalid Input. Either the input for accountId or BankingDetails was invalid.");
		throw new Exception("Invalid input.");
	}

	public BayUser addUser(BayUser user) throws EntityExistsException
	{
		logger.debug("BEGIN add user");
		addUserValidation(user);
		final BayUser newUser = userRepo.save(user);
		logger.debug("END add user");
		return newUser;
	}

	public BayUser findByScreenNameAndPassword(String screenName, String password) throws ObjectNotFoundException
	{
		final BayUser user = userRepo.findByScreenNameAndPassword(screenName, password);
		userValidation(user);
		return user;
	}

	public String getEmailAddressByScreenname(String screenName) throws Exception
	{
		final BayUser user = userRepo.findByScreenName(screenName);
		if (user == null)
		{
			logger.warn("No user found for username: " + screenName);
			throw new Exception("No user found for username: " + screenName);
		}
		return user.getEmail();
	}

	public Account addUserAccount(Account account, Long userId) throws ObjectNotFoundException, EntityExistsException
	{
		final BayUser user = userRepo.findById(userId);
		userValidation(user);
		if (user.getAccount() != null)
		{
			throw new EntityExistsException("UserAccount already exists");
		}
		account.setUser(user);
		if (account.getAddresses() != null && !account.getAddresses().isEmpty())
		{
			account.getAddresses().forEach(a -> a.setAccount(account));

			account.getAddresses().get(0).setIsPrimary(true);
		}
		final Account newAccount = accountRepo.saveAndFlush(account);
		return newAccount;
	}

	public Address updateUserAccountAddress(final Address address, final Long userId, final Long addressId)
			throws ObjectNotFoundException
	{
		final BayUser user = userRepo.findById(userId);
		userValidation(user);
		if (user.getAccount() == null)
		{
			throw new ObjectNotFoundException("The user [" + userId + "] does not have a userAccount");
		}

		final List<Address> updateCandidateList = user.getAccount().getAddresses().stream()
				.filter(addr -> addr.getId() == addressId).collect(Collectors.toList());
		final Address addressToUpdate = updateCandidateList.get(0);
		if (addressToUpdate == null)
		{
			throw new ObjectNotFoundException("The address [" + addressId + "] does not exist");
		}
		copyAddressInformation(addressToUpdate, address);
		final Account updatedAccount = accountRepo.saveAndFlush(user.getAccount());
		return updatedAccount.getAddresses().stream().filter(addr -> addr.getId() == addressId)
				.collect(Collectors.toList()).get(0);
	}

	public Account updateUserAccount(final Long userId, final Account updatedAccount) throws ObjectNotFoundException
	{
		final BayUser user = userRepo.findById(userId);
		userValidation(user);
		final Account originalAccount = user.getAccount();
		final Account mergedAccount = updateAccountInformation(originalAccount, updatedAccount);
		user.setAccount(mergedAccount);

		final Account result = userRepo.saveAndFlush(user).getAccount();

		return result;
	}

	private Account updateAccountInformation(Account original, Account update)
	{
		final List<Address> originalAddrList = original.getAddresses();
		final List<Address> updateAddrList = update.getAddresses();

		if (originalAddrList.size() == 0)
		{
			final Address addr = new Address();
			originalAddrList.add(addr);

		}
		for (int i = 0; i < originalAddrList.size(); i++)
		{

			final Address originalAddress = originalAddrList.get(i);
			final Address updateAddress = updateAddrList.get(i);

			originalAddress.setCity(updateAddress.getCity());
			originalAddress.setCountry(updateAddress.getCountry());
			originalAddress.setCounty(updateAddress.getCounty());
			originalAddress.setFullName(updateAddress.getFullName());
			originalAddress.setIsPrimary(updateAddress.getIsPrimary());
			originalAddress.setPhoneNumber(updateAddress.getPhoneNumber());
			originalAddress.setPostCode(updateAddress.getPostCode());
			originalAddress.setStreetAddress_1(updateAddress.getStreetAddress_1());
			originalAddress.setStreetAddress_2(updateAddress.getStreetAddress_2());

			if (originalAddress.getAccount() == null)
			{
				originalAddress.setAccount(original);
			}

		}

		PaymentAccount originalPaymentAccount = original.getPaymentAccount();
		originalPaymentAccount = update.getPaymentAccount();
		original.setPaymentAccount(originalPaymentAccount);

		return original;

	}

	public List<Account> getAllAccounts()
	{
		return accountRepo.findAll();
	}

	private void addUserValidation(BayUser user) throws EntityExistsException
	{
		BayUser dbUser = userRepo.findByEmail(user.getEmail());
		if (dbUser != null)
		{
			throw new EntityExistsException("User with email [" + dbUser.getEmail() + "] already exists");
		}
		dbUser = userRepo.findByScreenName(user.getScreenName());
		if (dbUser != null)
		{
			throw new EntityExistsException("User with username [" + dbUser.getScreenName() + "] already exists");
		}
	}

	private void userValidation(BayUser user) throws ObjectNotFoundException
	{
		if (user == null)
		{
			throw new ObjectNotFoundException("No user found");
		}

	}

	/**
	 * Move this functionality to the Address class
	 * 
	 * @param dbAddress
	 * @param address
	 * @return
	 */
	private Address copyAddressInformation(Address dbAddress, Address address)
	{
		dbAddress.setCity(address.getCity());
		dbAddress.setCountry(address.getCountry());
		dbAddress.setCounty(address.getCounty());
		dbAddress.setFullName(address.getFullName());
		dbAddress.setIsPrimary(address.getIsPrimary());
		dbAddress.setPhoneNumber(address.getPhoneNumber());
		dbAddress.setPostCode(address.getPostCode());
		dbAddress.setStreetAddress_1(address.getStreetAddress_1());
		dbAddress.setStreetAddress_2(address.getStreetAddress_2());
		return dbAddress;
	}

}
