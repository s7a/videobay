package io.dublinbrains.videobay.service.advert;

import java.time.Instant;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertRepository;
import io.dublinbrains.videobay.model.advert.AdvertState;
import io.dublinbrains.videobay.model.advert.Country;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.model.user.UserRepository;

@Service
public class AdvertService
{

	Logger logger = LogManager.getLogger(AdvertService.class.getName());

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private AdvertRepository advertRepo;

	public Advert cancelAdvertById(Long adsId) throws ObjectNotFoundException
	{
		final Advert advert = getAdvertById(adsId);
		advert.setState(AdvertState.CANCELD);
		return advertRepo.saveAndFlush(advert);
	}

	public List<Advert> updateAdvertProductInformationForUser(Long userId, Advert updatedAdvert)
			throws ObjectNotFoundException
	{
		final BayUser user = getUserById(userId);
		if (!user.getAdverts().stream().filter(ad -> ad.getId() == updatedAdvert.getId()).findAny().isPresent())
		{
			throw new ObjectNotFoundException("Can't update advert with id[" + updatedAdvert.getId() + "] for user["
					+ user.getId() + "] since it dosen't belong to him");
		}
		user.getAdverts().stream().filter(ad -> ad.getId() == updatedAdvert.getId()).findAny()
				.ifPresent(ad -> updateAdvertProductInformation(ad, updatedAdvert));
		final BayUser dbUser = userRepo.saveAndFlush(user);
		return dbUser.getAdverts();
	}

	private void updateAdvertProductInformation(Advert original, Advert update)
	{
		original.setCity(update.getCity());
		original.setCountry(update.getCountry());
		original.setCounty(update.getCounty());
		original.setDescription(update.getDescription());
		original.setPrice(update.getPrice());
		original.setTitle(update.getTitle());
	}

	public void deleteFavoriteForUser(Long userId, Long adsId) throws ObjectNotFoundException
	{
		final BayUser user = getUserById(userId);
		if (user.getFavoriteAds() == null || !user.getFavoriteAds().contains(adsId))
		{
			throw new ObjectNotFoundException("Can't delete favorite with id[" + adsId + "] from user[" + user.getId()
					+ "] since it was never favorized");
		}
		user.getFavoriteAds().remove(adsId);
		userRepo.saveAndFlush(user);
	}

	public List<Advert> getFavoriteForUser(Long userId) throws ObjectNotFoundException
	{
		final BayUser user = getUserById(userId);
		return advertRepo.findByRangeOfIds(user.getFavoriteAds());

	}

	public void addFavoriteForUser(Long userId, Long adsId) throws ObjectNotFoundException
	{
		final BayUser user = getUserById(userId);
		user.getFavoriteAds().add(adsId);
		getAdvertById(adsId);
		userRepo.saveAndFlush(user);
	}

	public Advert addAdvertForUser(Long userId, Advert advert) throws ObjectNotFoundException, IllegalArgumentException
	{
		final BayUser dbUser = getUserById(userId);
		advert.setCreated(Instant.now().toEpochMilli());
		advert.setState(AdvertState.PENDING);
		advert.setUser(dbUser);

		final Advert dbAdvert = advertRepo.save(advert);

		return dbAdvert;
	}

	private BayUser getUserById(Long userId) throws ObjectNotFoundException
	{
		final BayUser dbUser = userRepo.findById(userId);
		if (dbUser == null)
		{
			throw new ObjectNotFoundException("User with Id[" + userId + "] not found in DB");
		}
		return dbUser;
	}

	public Advert getAdvertById(Long adsId) throws ObjectNotFoundException
	{
		final Advert ad = advertRepo.findOne(adsId);
		if (ad == null)
		{
			throw new ObjectNotFoundException("Advert with Id[" + adsId + "] not found in DB");
		}
		return ad;
	}

	public List<Advert> getAdvertsForUser(Long userId) throws ObjectNotFoundException
	{
		getUserById(userId);
		final List<Advert> adverts = advertRepo.findByUserId(userId);
		return adverts;
	}

	public List<Advert> getAdvertsForUserByState(Long userId, AdvertState state) throws ObjectNotFoundException
	{
		getUserById(userId);
		final List<Advert> adverts = advertRepo.findByUserIdAndState(userId, state);
		return adverts;
	}

	public List<Advert> getAdvertsByCountryAndState(Country county, AdvertState state)
	{
		final List<Advert> adverts = advertRepo.findByCountryAndState(county, state);
		return adverts;
	}
}
