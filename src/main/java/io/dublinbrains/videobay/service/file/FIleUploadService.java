package io.dublinbrains.videobay.service.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertRepository;
import io.dublinbrains.videobay.model.advert.AdvertState;
import io.dublinbrains.videobay.service.video.VideoPostProcessingService;
import io.dublinbrains.videobay.service.video.VideoPostProcessingException;

@Service
public class FIleUploadService {
	
	@Autowired
	private AdvertRepository advertRepro;
	
	@Autowired
	VideoPostProcessingService postProcessingService;

	private final String nginxServerAddress;

	private final Path basePath;
	
	private final String targetWidth;
	
	private final String targetHeight;

	@Autowired
	public FIleUploadService(@Value("${base.content.path}") String basePathStr, @Value("${nginx.server.address}") String nginxServerAddress,
							 @Value("${target.video.width}") String targetWidth, @Value("${target.video.height}") String targetHeight) {
		this.basePath = Paths.get(basePathStr);
		this.nginxServerAddress = nginxServerAddress;
		this.targetWidth = targetWidth;
		this.targetHeight = targetHeight;

	}

	public Advert store(MultipartFile file, Long advertId) throws StorageException, ObjectNotFoundException, NumberFormatException, VideoPostProcessingException {
		Advert dbAdvert = advertRepro.findOne(advertId);
		if (dbAdvert == null) {
			throw new ObjectNotFoundException("Advert with specified ID[" + advertId + "] not found");
		}
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			validateIncomingFile(file, filename);
			String absulutePathStr = createDirs(basePath.toString(), advertId);
			Path absolutePath = Paths.get(absulutePathStr);
			Files.copy(file.getInputStream(), absolutePath.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
			dbAdvert.setState(AdvertState.OPEN);
			String dowsampledVideoPath = postProcessingService.downSample(absolutePath.resolve(filename).toString(), 
					Integer.valueOf(targetWidth), Integer.valueOf(targetHeight));
			String snippetPath = postProcessingService.generateSnippet(dowsampledVideoPath);
			dbAdvert.setVideoUrl(constructNginxFileUrl(snippetPath, nginxServerAddress));
			postProcessingService.moveMovAtom(snippetPath);
			advertRepro.saveAndFlush(dbAdvert);
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}

		return dbAdvert;
	}

	private void validateIncomingFile(MultipartFile file, String filename) throws StorageException {
		if (file.isEmpty()) {
			throw new StorageException("Failed to store empty file " + filename);
		}
		if (filename.contains("..")) {
			// This is a security check
			throw new StorageException("Cannot store file with relative path outside current directory " + filename);
		}
	}

	private String createDirs(String basePathStr, Long advertId) {
		StringJoiner joiner = new StringJoiner("/");
		joiner.add(basePathStr);
		joiner.add(advertId.toString());
		File dir = new File(joiner.toString());
		dir.mkdirs();
		return dir.getAbsolutePath();
	}

	private String constructNginxFileUrl(String absolutePathStr, String nginxServerAddress) {
		StringBuilder nginxUrlBuilder = new StringBuilder(absolutePathStr);
		//This replacement is based on the base.content.path property
		nginxUrlBuilder.replace(0, 13, "/");
		nginxUrlBuilder.insert(0, nginxServerAddress);
		return nginxUrlBuilder.toString();

	}

}
