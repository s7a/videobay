package io.dublinbrains.videobay.service.file;

public class StorageException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3299232682995492712L;

	public StorageException(String message) {
		super(message);
	}

	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}

}
