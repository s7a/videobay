package io.dublinbrains.videobay.service.subscription;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import io.dublinbrains.videobay.model.subscription.Subscription;
import io.dublinbrains.videobay.model.subscription.SubscriptionRepository;
import io.dublinbrains.videobay.utils.EmailUtils;

@Service
public class SubscriptionService
{
	public final static String subject = "Willkommen auf unserem Weg zur Beta!";

	Logger logger = LogManager.getLogger(SubscriptionService.class.getName());

	@Autowired
	SubscriptionRepository subscriptionRepo;
	
	@Autowired
	EmailUtils emailUtils;
	
	public Subscription saveSubscription(String email) throws Exception
	{
		if (!StringUtils.isEmpty(email))
		{
			Subscription returnValue = new Subscription();
			
			if(subscriptionRepo.findByEmail(email) == null) {
				returnValue.setEmail(email);
				emailUtils.sendEmail(email, subject, "Hi, \n\n toll, Du teilst unsere Begeisterung für Picturize – dem ersten videobasierten online Marktplatz. \n\n" +
"Bald ist es soweit und Du kannst auf unserem brandneuen Marktplatz deine nicht mehr benutzten Sachen loswerden. Freu‘ dich auf ein einzigartiges Verkaufs- und Kauferlebnis.\n\n" +
"Damit Du unter den ersten bist, die auf unserem Marktplatz aktiv werden, halten wir dich über unsere Entwicklung auf dem Laufenden und werden dir frühzeitig berichten wann es tatsächlich losgeht.\n\n" +
"Bis dahin heißt es, Unbenutztes für den Verkauf bei uns sammeln!\n\n" +
"Wir freuen uns auf dich!\n\n" +
"Dein Picturize Team\n" +
"Sven, Artur & Marcel \n\n\n"
+ "Solltest du es dir anders überlegen kannst du dich hier wieder abmelden:\n"
+ "www.picturize.info/deleteSubscription.html");
				return subscriptionRepo.save(returnValue); 
			}
			
			emailUtils.sendEmail(email, subject, "Hi,\n\n"
					+ "da bist du ja wieder, wir können es auch kaum erwarten, aber es dauert leider noch ein wenig. \n"+
					"Bis dahin heißt es, Unbenutztes für den Verkauf bei uns sammeln!\n\n" +
					"Wir freuen uns auf dich!\n\n" +
					"Dein Picturize Team\n" +
					"Sven, Artur & Marcel \n\n\n"
					+ "Solltest du es dir anders überlegen kannst du dich hier wieder abmelden:\n"
					+ "www.picturize.info/deleteSubscription.html");
			return returnValue;
			
		}
		logger.warn("Invalid Input. The input for email was invalid.");
		throw new Exception("Invalid input.");
	}
	
	public boolean deleteSubscription(String email) {
		Subscription subscriptionToDelete = subscriptionRepo.findByEmail(email);
		
		if(subscriptionToDelete == null)
			return false;
			
		subscriptionRepo.delete(subscriptionToDelete);
		return true;
	}

	public List<Subscription> getAllSubscriptions() {
		return subscriptionRepo.findAll();
	}
}
