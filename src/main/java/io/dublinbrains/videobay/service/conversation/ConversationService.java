package io.dublinbrains.videobay.service.conversation;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.conversation.Conversation;
import io.dublinbrains.videobay.model.conversation.ConversationRepository;
import io.dublinbrains.videobay.service.advert.AdvertService;

@Service
public class ConversationService {
	
	private Logger logger = LogManager.getLogger(ConversationService.class.getName());
	
	@Autowired
	ConversationRepository conversationRepo;
	
	@Autowired
	AdvertService advertService;
	
	
	public Conversation getById(final Long id) throws ObjectNotFoundException{
		Conversation dbConversation = conversationRepo.findOne(id);
		if (dbConversation == null)
		{
			throw new ObjectNotFoundException("Conversation with Id[" + id + "] not found");
		}
		return dbConversation;
	}
	
	public List<Conversation> getByAdvertId(final Long advertId) throws ObjectNotFoundException{
		List<Conversation> convList = conversationRepo.findByAdvertId(advertId);
		if(convList.isEmpty() || convList == null){
			throw new ObjectNotFoundException("Conversation with advertId[" + advertId + "] not found");
		}
		return convList;
	}
	
	public Conversation addByAdvertId(final Conversation conversation, final Long advertId) throws ObjectNotFoundException{
		final Advert advert = advertService.getAdvertById(advertId);
		conversation.setAdvert(advert);
		Conversation dbConv = conversationRepo.saveAndFlush(conversation);
		return dbConv;
	}
	
	public Conversation updateById(final Conversation conversation, final Long advertId ,final Long id) throws ObjectNotFoundException{
		Advert advert = advertService.getAdvertById(advertId);
		if (!(advert.getConversations().stream().filter(conv -> conv.getId() == id).findAny().isPresent())){
			logger.debug("Conversation with advertId[" + id + "] not found");
			throw new ObjectNotFoundException("Conversation with advertId[" + id + "] not found");
		}
		Conversation dbConv = this.getById(id);
		dbConv = updateContent(dbConv, conversation);
		conversationRepo.saveAndFlush(dbConv);
		return dbConv;
	}
	
	private Conversation updateContent(final Conversation dbConv, Conversation conv){
		dbConv.setQuestion(conv.getQuestion());
		dbConv.setAnswer(conv.getAnswer());
		
		return dbConv;
		
	}

}
