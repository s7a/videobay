package io.dublinbrains.videobay.service.payment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCollection;
import com.stripe.model.Token;

import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.payment.CreditCard;
import io.dublinbrains.videobay.model.payment.Payment;
import io.dublinbrains.videobay.model.payment.StripeCharge;

@Service
public class PaymentService {

	Logger logger = LogManager.getLogger(PaymentService.class.getName());
	
	private static final String TEST_STRIPE_SECRET_KEY = "sk_test_Ux4WRax62EyZ7hQzrgIm74Em";

	public PaymentService() {
		Stripe.apiKey = TEST_STRIPE_SECRET_KEY;
	}

	public String stripeProcessCreditCard(CreditCard creditCard, String stripeUser) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		
		// Token is created using Checkout or Elements!
		// Get the payment token ID submitted by the form:
		Map<String, Object> tokenParams = new HashMap<String, Object>();
		Map<String, Object> cardParams = new HashMap<String, Object>();
		cardParams.put("number", creditCard.getCardNumber());
		cardParams.put("exp_month", creditCard.getExpirationMonth());
		cardParams.put("exp_year", creditCard.getExpirationYear());
		cardParams.put("cvc", creditCard.getCvv());
		tokenParams.put("card", cardParams);
		// TODO do we have to pass the customer here?
		
		Token token = Token.create(tokenParams);
		
		// card has to be saved afterwards
		Customer customer = Customer.retrieve(stripeUser);
		try{
			customer.getSources().retrieve(token.getId());
		}catch(InvalidRequestException e){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("source", token.getId());
			customer.getSources().create(params);
		}
		
		return token.getCard().getId();
	}
	
	public String checkIfStripeUserExists(Account account) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		
		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("limit", "30");
		customerParams.put("email", account.getUser().getEmail());
		
		CustomerCollection stripeCustomers = Customer.list(customerParams);
		
		List<Customer> customers = null;
		if(stripeCustomers != null)
			customers = stripeCustomers.getData();
			
		for (Customer customer : customers) {
			if(customer.getDescription().equals(account.getUser().getFirstName() + " " + account.getUser().getSurName()))
				return customer.getId();
		}
		
		return null;
	}
	
	public String createStripeCustomer(Account account, String cardToken) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
			
		String stripeCustomerId = checkIfStripeUserExists(account);
		
		if(stripeCustomerId == null) {
			Map<String, Object> customerParams = new HashMap<String, Object>();
			customerParams.put("description", account.getUser().getFirstName() + " " + account.getUser().getSurName());
			customerParams.put("email", account.getUser().getEmail());
			if(cardToken != null)
				customerParams.put("source", cardToken);
					
			String id = null;
					
			try { 
					// Create customer
					Customer stripeCustomer = Customer.create(customerParams);
					id = stripeCustomer.getId();
					System.out.println(stripeCustomer);
				} catch (CardException e) {
					// Transaction failure
					System.out.println(e);
				} catch (RateLimitException e) {
					// Too many requests made to the API too quickly
					System.out.println(e);
				} catch (InvalidRequestException e) {
					// Invalid parameters were supplied to Stripe's API
					System.out.println(e);
				} catch (AuthenticationException e) {
					// Authentication with Stripe's API failed (wrong API key?)
					System.out.println(e);
				} catch (APIConnectionException e) {
					// Network communication with Stripe failed
					System.out.println(e);
				} catch (StripeException e) {
					// Generic error
					System.out.println(e);
				} catch (Exception e) {
					// Something else happened unrelated to Stripe
					System.out.println(e);
				}
				
			return id;
		}
		return stripeCustomerId;
	}

	public Charge chargeCreditCard(Payment payment, Advert advert, String creditCardToken) {
					
		// Stripe requires the charge amount to be in cents
		int chargeAmountCents = advert.getPrice().intValue() * 100;

		Account account = payment.getAccountBuyer();

		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("amount", chargeAmountCents);
		// Die Currency am Payment halten?
		chargeParams.put("currency", "usd");
		chargeParams.put("description", payment.getAdvert().getTitle());
		
		StripeCharge sC = new StripeCharge();
		for (StripeCharge charge : account.getStripeCharges()) {
			//if(charge.getStripeCreditCardToken() != null && charge.getStripeCreditCardToken().equals(creditCardToken))
				sC = charge;
		}
		chargeParams.put("customer", sC.getStripeUserId());
					
		try {
				// Submit charge to credit card 
				return Charge.create(chargeParams);
		    } catch (CardException e) {
		    	// Transaction was declined
		    	System.out.println("Status is: " + e.getCode());
		    	System.out.println("Message is: " + e.getMessage());
			} catch (RateLimitException e) {
				// Too many requests made to the API too quickly
				System.out.println("Status is: " + e.getStatusCode());
		    	System.out.println("Message is: " + e.getMessage());
			} catch (InvalidRequestException e) {
				// Invalid parameters were supplied to Stripe's API
				System.out.println("Status is: " + e.getStatusCode());
		    	System.out.println("Message is: " + e.getMessage());
		    } catch (AuthenticationException e) {
		    	// Authentication with Stripe's API failed (wrong API key?)
		    	System.out.println("Status is: " + e.getStatusCode());
		    	System.out.println("Message is: " + e.getMessage());
			} catch (APIConnectionException e) {
				// Network communication with Stripe failed
				System.out.println("Status is: " + e.getStatusCode());
		    	System.out.println("Message is: " + e.getMessage());
			 } catch (StripeException e) {
				 // Generic error
				 System.out.println("Status is: " + e.getStatusCode());
			    	System.out.println("Message is: " + e.getMessage());
			} catch (Exception e) {
				// Something else happened unrelated to Stripe
				System.out.println("Status is: ");
		    	System.out.println("Message is: " + e.getMessage());
			}
		return null;	
	}
}
