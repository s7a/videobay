package io.dublinbrains.videobay.service.video;

public interface VideoPostProcessingService {
	
	public void moveMovAtom(String filePath);
	
	public String downSample(String filePath, Integer width, Integer height) throws VideoPostProcessingException;
	
	public String generateSnippet(String filePath) throws VideoPostProcessingException;
}
