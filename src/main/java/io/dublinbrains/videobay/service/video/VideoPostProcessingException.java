package io.dublinbrains.videobay.service.video;

public class VideoPostProcessingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3530890589575733307L;
	
	public VideoPostProcessingException(String message) {
		super(message);
	}

	public VideoPostProcessingException(String message, Throwable cause) {
		super(message, cause);
	}
	

}
