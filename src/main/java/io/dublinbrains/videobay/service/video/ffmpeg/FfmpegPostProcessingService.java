package io.dublinbrains.videobay.service.video.ffmpeg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import io.dublinbrains.videobay.service.video.VideoPostProcessingService;
import io.dublinbrains.videobay.service.video.VideoPostProcessingException;

@Service
@Profile({"prod", "test"})
public class FfmpegPostProcessingService implements VideoPostProcessingService
{

	private Logger logger = LogManager.getLogger(FfmpegPostProcessingService.class.getName());
	private static final String RESOLUTION_SHELL_CMD_TEMPLATE = "ffprobe -i ${filePath} 2>&1 | grep -oP 'Stream .*, \\K[0-9]+x[0-9]+'";
	private static final String FPS_SHELL_CMD_TEMPLATE = "ffmpeg -i ${filePath} 2>&1 | sed -n \"s/.*, \\(.*\\) fp.*/\\1/p\"";
	private static final String DURATION_SHELL_CMD_TEMPLATE = "ffmpeg -i ${filePath} 2>&1 | grep Duration | awk '{print $2}' | tr -d ,";
	private static final String DOWN_SAMPLED_FILE_SUFFIX = "_small1.mp4";
	private static final String SNIPPET_FILE_SUFFIX = "_snippet.mp4";
	
	@Value("${snippet.duration}")
	private String snippetDuration;
	
	@Value("${snippet.pts}")
	private Double snippetPts;

	@Override
	public void moveMovAtom(String filePath)
	{
		logger.debug("Trying FFMPEG to move MovAtom metadata");

	}

	@Override
	public String downSample(String filePath, Integer targetWidth, Integer targetHeight) throws VideoPostProcessingException
	{
		logger.debug("Trying FFMPEG to downsample to {} x {} in {}", targetWidth, targetHeight, filePath);
		String downSampledVideoPath = filePath;
		try {
			Integer[] widthHeight  = getVideoResolution(filePath);
			if(isToDownSample(widthHeight[0], widthHeight[1], targetWidth, targetHeight)){
				logger.info("Beginning to downsample");
				downSampledVideoPath = ffmpegDownSample(filePath);
				logger.debug("Downsampled video => {}",downSampledVideoPath);
				
			}
		} catch (IOException | InterruptedException e) {
			// TODO => create Video Processing exception throw here and register in HTTP error handler
			throw new VideoPostProcessingException("Error in downsampling video", e);
		}
		return downSampledVideoPath;
	}
	
	@Override
	public String generateSnippet(String filePath) throws VideoPostProcessingException {
		logger.debug("Generating snippet for {}", filePath);
		Path fullVideoPath = Paths.get(filePath);
		Path videoDirectory = fullVideoPath.getParent();
		String snippetFileName = "";
		try {
			String startTimeStamp = determineSnippetStartTimeStamp(filePath);
			Long snippetDurationInSec = transformFfmpegDurationToSeconds(snippetDuration);
			Long snippetStartTimeInSec = transformFfmpegDurationToSeconds(startTimeStamp);
			if(snippetStartTimeInSec < snippetDurationInSec){
				logger.debug("Snippet generation aborted, video length insufficient for snippet duration of {}", snippetDuration);
				snippetFileName = constructConvertedFileName(fullVideoPath, SNIPPET_FILE_SUFFIX);
				
				// TODO => Haevy refactoring needed, move alle ProcessBuilder + Process into one method !!!!!!!!
				final ProcessBuilder builder = new ProcessBuilder();
				builder.command(removeAudioShellCommand(fullVideoPath, snippetFileName));
				builder.redirectErrorStream(true);
				
				String line = "";
				final Process process = builder.start();
				final StringBuilder sb = new StringBuilder();
				final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
				while ((line = reader.readLine()) != null)
				{
					sb.append(line);
				}
				logger.info("Command output => {}", sb.toString());
				
				return videoDirectory + "/" + snippetFileName;
			}
			snippetFileName = constructConvertedFileName(fullVideoPath, SNIPPET_FILE_SUFFIX);
			Double videoFps = getVideoFps(filePath);
			final ProcessBuilder builder = new ProcessBuilder();
			builder.command(generateSnippetShellCommand(fullVideoPath, startTimeStamp, snippetDuration, snippetFileName, videoFps));
			builder.redirectErrorStream(true);
	
			String line = "";
			final Process process = builder.start();
			final StringBuilder sb = new StringBuilder();
			final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			while ((line = reader.readLine()) != null)
			{
				sb.append(line);
			}
			logger.info("Command output => {}", sb.toString());
		} catch (IOException e) {
			throw new VideoPostProcessingException("Error in snippet generation", e);
		}
		return videoDirectory + "/" + snippetFileName;
	}
	
	private String determineSnippetStartTimeStamp(String filePath) throws IOException{
		String videoDuration = getVideoDuration(filePath);
		String halfDuration = computeHalfDuration(videoDuration);
		return halfDuration;
	}
	
	/**
	 * @param duration in FFPEG format HH:MM:SS
	 * @return half of inputs duration in FFMEG format HH:MM:SS
	 * 
	 * Be aware since this method is just a quick hack in order to get HH:MM:SS format
	 * Method relies on Java LocalTime API and there for will be limited to 23:59:59 MAX duration 
	 */
	private String computeHalfDuration(String duration){
		final Long seconds = transformFfmpegDurationToSeconds(duration);
		LocalTime lt = LocalTime.MIN.plusSeconds(seconds/2) ;
		DateTimeFormatter f = DateTimeFormatter.ofPattern("HH:mm:ss") ;
		String ffmegHalfDuration = lt.format(f) ;
		logger.info("Half duration was computed as {}" , ffmegHalfDuration);
		
		return ffmegHalfDuration;
	}
	
	private Long transformFfmpegDurationToSeconds(String ffmpegDuration){
		String[] times = StringUtils.split(ffmpegDuration, ":");
		Long seconds = Long.valueOf(times[2]);
		seconds += Long.valueOf(times[1]) * 60;
		seconds += Long.valueOf(times[0]) * 3600;
		logger.info("FFMPEG duration {} is transformed to {} seconds", ffmpegDuration, seconds);
		
		return seconds;
	}
	
	/**
	 * @param filePath
	 * @return video duration in FFPEG output HH:MM:SS
	 * @throws IOException
	 */
	private String getVideoDuration(String filePath) throws IOException{
		final ProcessBuilder builder = new ProcessBuilder();
		builder.command(generateVideoDurationShellArguments(filePath));
		builder.redirectErrorStream(true);
		
		final Process process = builder.start();
		
		final StringBuilder sb = new StringBuilder();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

		String line = "";
		while ((line = reader.readLine()) != null)
		{
			sb.append(line);
		}
		String videoDuration;
		if(StringUtils.contains(sb.toString(), '.')){
			videoDuration =  StringUtils.split(sb.toString(), ".")[0];
		}
		else{
			videoDuration = sb.toString();
		}
		
		logger.info("Video duration => {}", videoDuration);
		
		
		return videoDuration;
		
	}
	
	private Double getVideoFps(String filePath) throws IOException{
		final ProcessBuilder builder = new ProcessBuilder();
		builder.command(generateVideoFpsShellArguments(filePath));
		builder.redirectErrorStream(true);
		
		final Process process = builder.start();
		
		final StringBuilder sb = new StringBuilder();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		
		String line = "";
		while ((line = reader.readLine()) != null)
		{
			sb.append(line);
		}
		String videoFpsStr = sb.toString();
		Double videoFps = Double.valueOf(videoFpsStr);
		logger.info("Video Fps => {}", videoFps);
		
		
		return videoFps;
		
	}
	
	private String[] generateVideoDurationShellArguments(final String filePath){
		Map<String, String> data = new HashMap<String, String>();
		data.put("filePath", filePath);
		final String resShellCmd = StrSubstitutor.replace(DURATION_SHELL_CMD_TEMPLATE, data);
		logger.info("ResolutionShellCmd => {}", resShellCmd);
		String[] commandArguments = {"/bin/bash", "-c", resShellCmd};
		return commandArguments;
	}
	
	private String[] generateVideoFpsShellArguments(final String filePath){
		Map<String, String> data = new HashMap<String, String>();
		data.put("filePath", filePath);
		final String fpsShellCmd = StrSubstitutor.replace(FPS_SHELL_CMD_TEMPLATE, data);
		logger.info("FpsShellCmd => {}", fpsShellCmd);
		String[] commandArguments = {"/bin/bash", "-c", fpsShellCmd};
		return commandArguments;
	}
	
	private String[] removeAudioShellCommand(Path originalFilePath, String snippetFileName){
		String[] cmd = {"ffmpeg","-i", originalFilePath.toString(), "-c", "copy", "-an", originalFilePath.getParent().toString()+"/"+snippetFileName};
		return cmd;
	}
	
	private String[] generateSnippetShellCommand(Path originalFilePath,String beginTimeStamp, String duration, String snippetFileName, Double videoFps){
		Double targetFps = videoFps / snippetPts;
		String[] cmd = {"ffmpeg","-ss", beginTimeStamp, "-i", originalFilePath.toString(), "-vf", "setpts="+snippetPts.toString()+"*PTS", "-r" , targetFps.toString(),
						 "-an", "-t", duration, "-strict", "-2", originalFilePath.getParent().toString()+"/"+snippetFileName};
		
		return cmd;
	}
	
	private Integer[] getVideoResolution(final String filePath) throws IOException{
		final ProcessBuilder builder = new ProcessBuilder();
		builder.command(generateResolutionShellArguments(filePath));
		builder.redirectErrorStream(true);
		
		final Process process = builder.start();
		Integer[] widthHeight = extractResultionFromProcessResponse(process);
		logger.debug("Clip {} has a resolution of {}x{}", filePath, widthHeight[0], widthHeight[1]);

		
		return widthHeight;
		
	}
	
	private String[] generateResolutionShellArguments(final String filePath){
		Map<String, String> data = new HashMap<String, String>();
		data.put("filePath", filePath);
		final String resShellCmd = StrSubstitutor.replace(RESOLUTION_SHELL_CMD_TEMPLATE, data);
		logger.info("ResolutionShellCmd => {}", resShellCmd);
		String[] commandArguments = {"/bin/bash", "-c", resShellCmd};
		return commandArguments;
	}
	
	/**
	 * @param p
	 * @return Array with [0] = width and [1] = height 
	 * @throws IOException 
	 */
	private Integer[] extractResultionFromProcessResponse(final Process p) throws IOException
	{
		final StringBuilder sb = new StringBuilder();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		while ((line = reader.readLine()) != null)
		{
			sb.append(line);
		}
		logger.info("Command output => {}", sb.toString());
		String[] widthHeightString = StringUtils.split(sb.toString(), "x");
		Integer[] widthHeight = {Integer.valueOf(widthHeightString[0]), Integer.valueOf(widthHeightString[1])};
		return widthHeight;
	}
	
	private boolean isToDownSample(Integer width, Integer height, Integer targetWidth, Integer targetHeight){
		if(width > targetWidth && height > targetHeight){
			logger.debug("Need to downsample");
			return true;
		}else{
			logger.debug("No downsampling requiered");
			return false;
		}
	}
	
	private String ffmpegDownSample(String filePath) throws IOException, InterruptedException{
		Path fullVideoPath = Paths.get(filePath);
		Path videoDirectory = fullVideoPath.getParent();
		logger.debug("Operating in directory => {}", videoDirectory.toString());
		final String downSampledFileName = constructConvertedFileName(fullVideoPath, DOWN_SAMPLED_FILE_SUFFIX);
		final ProcessBuilder builder = new ProcessBuilder();
		String[] commandArguments = {"ffmpeg", "-i", fullVideoPath.toString(), "-s" , "hd480" , "-c:v", "libx264", "-crf", "23", "-c:a", "aac", "-strict", "-2", 
				videoDirectory.toString() + "/" + downSampledFileName};
		builder.command(commandArguments);
		builder.redirectErrorStream(true);
		
		final Process process = builder.start();
		process.waitFor();
		logger.info(" ExitValue => {}", process.exitValue());
		final StringBuilder sb = new StringBuilder();
		final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

		String line = "";
		while ((line = reader.readLine()) != null)
		{
			sb.append(line + "/n");
		}
		logger.info("Process result => {}", sb.toString());
		
		return videoDirectory.toString() + "/" + downSampledFileName;
	}
	
	private String constructConvertedFileName(Path original, String fileNameSuffix){
		String originalFileName = original.getFileName().toString();
		Integer fileExtensionPosition = originalFileName.indexOf('.');
		return originalFileName.substring(0, fileExtensionPosition).concat(fileNameSuffix);
		
	}

}
