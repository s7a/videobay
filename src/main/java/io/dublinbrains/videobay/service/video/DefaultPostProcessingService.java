package io.dublinbrains.videobay.service.video;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import io.dublinbrains.videobay.service.video.ffmpeg.FfmpegPostProcessingService;

@Service
@Profile({"dev"})
public class DefaultPostProcessingService implements VideoPostProcessingService {

	Logger logger = LogManager.getLogger(FfmpegPostProcessingService.class.getName());
	
	@Override
	public void moveMovAtom(String filePath) {
		logger.debug("Trying Default to move MovAtom metadata in {}", filePath);
	}

	@Override
	public String downSample(String filePath, Integer width, Integer height) {
		logger.debug("Trying Default to downsample to {} x {} in {}", width, height, filePath);
		return "DEFAULT";
		
	}

	@Override
	public String generateSnippet(String filePath) {
		// TODO Auto-generated method stub
		return null;
	}

}
