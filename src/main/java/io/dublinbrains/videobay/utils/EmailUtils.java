package io.dublinbrains.videobay.utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.dublinbrains.videobay.service.subscription.SubscriptionService;
import io.dublinbrains.videobay.service.user.UserService;

@Component
public class EmailUtils {

	@Autowired
	private UserService userService;
	
	public boolean sendEmail(String recepient, String subject, String text) throws Exception{
		
		final String username = "subscription@picturize.info";
		final String password = "Kaffee123!";
	
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.strato.de");
		props.put("mail.smtp.port", "587");
	
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
	
		try {
	
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("subscription@picturize.info"));
			
			String emailAddress;
			
			if(subject.equals(SubscriptionService.subject))
				emailAddress= recepient;
			else
			    emailAddress = userService.getEmailAddressByScreenname(recepient);
			
			
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(emailAddress));
			
			message.setSubject(subject);
			message.setText(text);
	
			Transport.send(message);
	
			return true;
	
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}
