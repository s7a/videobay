package io.dublinbrains.videobay.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.dublinbrains.videobay.exceptions.EntityExistsException;
import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.account.Address;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.service.user.UserService;
import io.dublinbrains.videobay.utils.EmailUtils;

@RestController
@EnableAutoConfiguration
@CrossOrigin
@RequestMapping(value = "/users", produces = "application/json")
public class UserController
{

	Logger logger = LogManager.getLogger(UserController.class.getName());

	@Autowired
	UserService userService;

	@Autowired
	private EmailUtils emailUtils;

	@PostMapping
	public ResponseEntity<BayUser> addUser(@RequestBody BayUser user) throws EntityExistsException
	{
		return new ResponseEntity<>(userService.addUser(user), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<BayUser> findByScreenNameAndPassword(@RequestParam("username") String screenName,
			@RequestParam("password") String password) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(userService.findByScreenNameAndPassword(screenName, password), HttpStatus.OK);
	}

	@GetMapping(path = "/resetpw/{screenname}")
	public ResponseEntity<?> sendPasswordResetEmail(@RequestParam("screenname") String screenName) throws Exception
	{
		if (emailUtils.sendEmail(screenName, "Setzen sie Ihr Passwort zurück",
				"Hier erhalten sie demnächst einen Link um Ihr PW zu reseten!"))
			return new ResponseEntity<>(HttpStatus.OK);

		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping("/{userId}/accounts")
	public ResponseEntity<Account> addUserAccount(@PathVariable("userId") Long userId, @RequestBody Account account)
			throws ObjectNotFoundException, EntityExistsException
	{
		return new ResponseEntity<>(userService.addUserAccount(account, userId), HttpStatus.CREATED);
	}

	// @PostMapping("/{accountId}/accounts")
	// public ResponseEntity<Account>
	// addAccountBankindetails(@PathVariable("accountId") Long accountId,
	// @RequestBody BankingDetails bankingDetails)
	// throws ObjectNotFoundException, EntityExistsException
	// {
	// try {
	// return new ResponseEntity<>(userService.saveBankingDetails(accountId,
	// bankingDetails), HttpStatus.CREATED);
	// } catch (Exception e) {
	// return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	// }
	// }

	@PutMapping("/{userId}/accounts")
	public ResponseEntity<Account> updateUserAccount(@PathVariable("userId") Long userId, @RequestBody Account account)
			throws ObjectNotFoundException
	{
		final Account result = userService.updateUserAccount(userId, account);

		if (result != null)
			return new ResponseEntity<>(result, HttpStatus.OK);

		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PutMapping("/{userId}/accounts/addresses/{addressId}")
	public ResponseEntity<Address> updateUserAccountAddress(@PathVariable("userId") Long userId,
			@PathVariable("addressId") Long addressId, @RequestBody Address address) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(userService.updateUserAccountAddress(address, userId, addressId), HttpStatus.OK);
	}

	@GetMapping("/accounts")
	public ResponseEntity<List<Account>> getAllUserAccounts()
	{
		return new ResponseEntity<>(userService.getAllAccounts(), HttpStatus.OK);

	}

}
