package io.dublinbrains.videobay.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.conversation.Conversation;
import io.dublinbrains.videobay.service.conversation.ConversationService;

@RestController
@CrossOrigin
@RequestMapping(value = "/adverts/{advertId}/conversations", produces = "application/json")
public class ConversationController {
	
	@Autowired
	ConversationService convService;
	
	@PostMapping("/")
	ResponseEntity<Conversation> addConversation(@RequestBody Conversation conversation, @PathVariable Long advertId) throws ObjectNotFoundException{
		return new ResponseEntity<Conversation>(convService.addByAdvertId(conversation, advertId), HttpStatus.CREATED);
	}
	
	@GetMapping("/")
	ResponseEntity<List<Conversation>> getConversations(@PathVariable Long advertId) throws ObjectNotFoundException{
		return new ResponseEntity<List<Conversation>>(convService.getByAdvertId(advertId), HttpStatus.OK);
		
	}
	
	@PutMapping("/{id}")
	ResponseEntity<Conversation> updateConversation(@RequestBody Conversation conversation ,@PathVariable Long advertId, @PathVariable Long id) throws ObjectNotFoundException{
		return new ResponseEntity<Conversation>(convService.updateById(conversation, advertId,id), HttpStatus.OK);
	}

}
