package io.dublinbrains.videobay.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;

import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.account.AccountRepository;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertRepository;
import io.dublinbrains.videobay.model.advert.AdvertState;
import io.dublinbrains.videobay.model.payment.CreditCard;
import io.dublinbrains.videobay.model.payment.Payment;
import io.dublinbrains.videobay.model.payment.StripeCharge;
import io.dublinbrains.videobay.service.payment.PaymentService;

@RestController
@CrossOrigin
@RequestMapping(value = "/payment", produces = "application/json")
public class PaymentController {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	AccountRepository accountRepo;
	
	@Autowired
	AdvertRepository advertRepo;

	@PostMapping(path = "/byCreditCard/buyer/{buyerAccountId}/advert/{advertId}")
	public ResponseEntity<HttpStatus> payWithCreditCard (@PathVariable("buyerAccountId") String buyerAccountId, 
			@PathVariable("advertId") String advertId, @RequestBody CreditCard creditCard) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
		
		// getting the buyer videobay account from the Database
		Account accountBuyer = accountRepo.findOne(Long.valueOf(buyerAccountId));
		if(accountBuyer == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		// check if there is already an existing stripeCustomerAccount for this User, if not create one
		String stripeUserId = processStripeUser(accountBuyer);
		if(stripeUserId == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		// creating the cardToken, these can only be used once -> create one on every call
		String cardToken = paymentService.stripeProcessCreditCard(creditCard, stripeUserId);
		
		// getting the advert from the database
		Advert advert = advertRepo.findOne(Long.valueOf(advertId));
		if(advert == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		// FIXME StripeCharge element muss gespeichert werden
		StripeCharge charge = new StripeCharge(stripeUserId);
		charge.setStripeCreditCardToken(cardToken);
		Payment payment = new Payment(advert.getUser().getAccount(), accountBuyer, advert, charge);
		// FIXME save the payment here? 
		
		com.stripe.model.Charge result = paymentService.chargeCreditCard(payment, advert, cardToken);
		
		if(result != null) {
			if(result.getStatus().equals("succeeded")){
				advert.setState(AdvertState.SOLD);
				advertRepo.save(advert);
				accountRepo.save(accountBuyer);
				return new ResponseEntity<>(HttpStatus.OK);
			}
			
			if(result.getStatus().equals("pending"))
				return new ResponseEntity<>(HttpStatus.PROCESSING);
				
			if(result.getStatus().equals("failed"))
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
			
		// hier brauchen wir einen Return code dafür das die Zahlung fehlgeschlagen ist
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private StripeCharge getTheRightChargeElement(Account accountBuyer, String cardToken) {
		for (StripeCharge charge : accountBuyer.getStripeCharges()) {
			if (charge.getStripeCreditCardToken().equals(cardToken))
				return charge;
		}
		
		// This statement should never be reached if the workflow does his job correvtly!
		return new StripeCharge();
	}

	private String processStripeUser(Account accountBuyer) throws AuthenticationException,
			InvalidRequestException, APIConnectionException, CardException, APIException {
		
		// This call will create a StripeCustomer if needed, or return the already existing one
		String stripeCustomerUserId = paymentService.createStripeCustomer(accountBuyer, null);
		
		if(stripeCustomerUserId == null)
			return null;
		
		// If no StripeCharges are saved in our model, we will add the first one.
		// If there are StripeCharges already, we will make sure they are up to date
		// TODO use apache utils for isEmpty check here
		if(accountBuyer.getStripeCharges() == null || accountBuyer.getStripeCharges().isEmpty()) {
			List<StripeCharge> charges = new ArrayList<>();
			StripeCharge charge = new StripeCharge(stripeCustomerUserId);
			charges.add(charge);
			accountBuyer.setStripeCharges(charges);
		} else {
			for (StripeCharge charge : accountBuyer.getStripeCharges()) {
				charge.setStripeUserId(stripeCustomerUserId);
			}
		}
		return stripeCustomerUserId;
	}
}
