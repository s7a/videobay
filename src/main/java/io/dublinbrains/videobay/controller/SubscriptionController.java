package io.dublinbrains.videobay.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.dublinbrains.videobay.model.subscription.Subscription;
import io.dublinbrains.videobay.service.subscription.SubscriptionService;

@RestController
@EnableAutoConfiguration
@CrossOrigin
@RequestMapping(value = "/supscriptions", produces = "application/json")
public class SubscriptionController
{

	Logger logger = LogManager.getLogger(SubscriptionController.class.getName());

	@Autowired
	SubscriptionService subscriptionService;
	
	@DeleteMapping("/{email}/delete")
	public ResponseEntity<HttpStatus> deleteSubscription(@PathVariable("email") String email)
	{
		if(subscriptionService.deleteSubscription(email))
			return new ResponseEntity<>(HttpStatus.OK);
		
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/{email}/add")
	public ResponseEntity<Subscription> addSubscription(@PathVariable("email") String email)
			throws Exception
	{
		Subscription addedSubscription = subscriptionService.saveSubscription(email);
		
		if(addedSubscription.getEmail().isEmpty())
			return new ResponseEntity<>(HttpStatus.OK);	
			
		return new ResponseEntity<>(addedSubscription, HttpStatus.CREATED);
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<Subscription>> getAllSubscription()
			throws Exception
	{
		return new ResponseEntity<>(subscriptionService.getAllSubscriptions(), HttpStatus.OK);
	}
}
