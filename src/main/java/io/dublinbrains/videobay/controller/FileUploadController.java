package io.dublinbrains.videobay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.service.file.FIleUploadService;
import io.dublinbrains.videobay.service.file.StorageException;
import io.dublinbrains.videobay.service.video.VideoPostProcessingException;

@RestController
@CrossOrigin
@RequestMapping(value = "/uploads", produces = "application/json")
public class FileUploadController {

	@Autowired
	FIleUploadService fileService;

	@PostMapping("/{advertId}")
	public ResponseEntity<?> handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable Long advertId) throws StorageException, ObjectNotFoundException,
																													        NumberFormatException, VideoPostProcessingException {
		return new ResponseEntity<>(fileService.store(file, advertId), HttpStatus.CREATED);
	}
}
