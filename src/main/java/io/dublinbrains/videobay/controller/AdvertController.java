package io.dublinbrains.videobay.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.dublinbrains.videobay.exceptions.ObjectNotFoundException;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertState;
import io.dublinbrains.videobay.model.advert.Country;
import io.dublinbrains.videobay.service.advert.AdvertService;

@RestController
@CrossOrigin
@RequestMapping(value = "/adverts", produces = "application/json")
public class AdvertController
{

	@Autowired
	AdvertService adverService;

	@PutMapping(path = "/{adsId}/cancellations")
	public ResponseEntity<Advert> cancelAdverById(@PathVariable Long adsId) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(adverService.cancelAdvertById(adsId), HttpStatus.OK);
	}

	@DeleteMapping(path = "/users/{userId}/favorites")
	public ResponseEntity<?> deleteFavoriteForUser(@RequestBody Long advertId, @PathVariable Long userId)
			throws ObjectNotFoundException
	{
		adverService.deleteFavoriteForUser(userId, advertId);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(path = "/users/{userId}/favorites")
	public ResponseEntity<?> addFavoriteByUser(@RequestBody Long advertId, @PathVariable Long userId)
			throws ObjectNotFoundException
	{
		adverService.addFavoriteForUser(userId, advertId);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping(path = "/users/{userId}/favorites")
	public ResponseEntity<List<Advert>> getFavoriteByUser(@PathVariable Long userId) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(adverService.getFavoriteForUser(userId), HttpStatus.OK);
	}

	@PostMapping(path = "/byUser/{userId}")
	public ResponseEntity<Advert> addAdvertByUser(@RequestBody Advert advert, @PathVariable Long userId)
			throws ObjectNotFoundException, IllegalArgumentException
	{
		return new ResponseEntity<>(adverService.addAdvertForUser(userId, advert), HttpStatus.CREATED);
	}

	@PutMapping(path = "/users/{userId}")
	public ResponseEntity<List<Advert>> updateAdvertsForUser(@RequestBody Advert advert, @PathVariable Long userId)
			throws ObjectNotFoundException
	{
		return new ResponseEntity<>(adverService.updateAdvertProductInformationForUser(userId, advert), HttpStatus.OK);

	}

	@GetMapping(path = "/users/{userId}")
	public ResponseEntity<List<Advert>> getAdvertsForUser(@PathVariable Long userId) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(adverService.getAdvertsForUser(userId), HttpStatus.OK);
	}

	@GetMapping(path = "/users/{userId}/state/{state}")
	public ResponseEntity<List<Advert>> getAdvertsForUserByState(@PathVariable Long userId,
			@PathVariable AdvertState state) throws ObjectNotFoundException
	{
		return new ResponseEntity<>(adverService.getAdvertsForUserByState(userId, state), HttpStatus.OK);
	}

	@GetMapping(path = "/countries/{country}")
	public ResponseEntity<List<Advert>> getAdvertsByCountryAndState(@PathVariable String country,
			@RequestParam(name = "state", required = true) String state)
	{
		try
		{
			final Country co = Country.fromString(country);
			final AdvertState as = AdvertState.fromValue(state);
			final List<Advert> adverts = adverService.getAdvertsByCountryAndState(co, as);
			return new ResponseEntity<>(adverts, HttpStatus.OK);
		}
		catch (final IllegalArgumentException e)
		{
			// We don't want to expose what we not support yet => Therefor we
			// will just return an empty list
			final List<Advert> empty = new ArrayList<>();
			return new ResponseEntity<>(empty, HttpStatus.BAD_REQUEST);
		}
	}

}
