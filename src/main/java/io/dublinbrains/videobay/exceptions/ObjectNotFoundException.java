package io.dublinbrains.videobay.exceptions;

public class ObjectNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5097668964438943941L;

	public ObjectNotFoundException(String msg) {
		super(msg);
	}

}
