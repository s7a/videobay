package io.dublinbrains.videobay.exceptions;

public class EntityExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7379911649959492378L;

	public EntityExistsException(String errorMsg) {
		super(errorMsg);
	}
}
