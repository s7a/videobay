package io.dublinbrains.videobay.exceptions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import io.dublinbrains.videobay.service.file.StorageException;
import io.dublinbrains.videobay.service.video.VideoPostProcessingException;

@ControllerAdvice
public class GlobalRestExceptionHandler {

	Logger logger = LogManager.getLogger(GlobalRestExceptionHandler.class.getName());
	
	@ExceptionHandler(value = { EntityExistsException.class, ObjectNotFoundException.class, IllegalArgumentException.class, StorageException.class })
	protected ResponseEntity<String> handleConflict(Exception ex, WebRequest request) {
		logger.warn(ex.getMessage());
		return new ResponseEntity<>(ex.getMessage(), determineHttpStatusCode(ex));
	}

	private HttpStatus determineHttpStatusCode(Exception ex) {
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		if (ex instanceof EntityExistsException)
			status = HttpStatus.CONFLICT;
		if (ex instanceof ObjectNotFoundException)
			status = HttpStatus.NO_CONTENT;
		if (ex instanceof IllegalArgumentException)
			status = HttpStatus.BAD_REQUEST;
		if(ex instanceof StorageException)
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		if(ex instanceof VideoPostProcessingException)
			status = HttpStatus.INTERNAL_SERVER_ERROR;

		return status;
	}
}
