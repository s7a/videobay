package io.dublinbrains.videobay;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertRepository;
import io.dublinbrains.videobay.model.advert.AdvertState;
import io.dublinbrains.videobay.model.advert.Country;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.model.user.UserRepository;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { VideoBayAppTest.class })
@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class AdvertReproTest extends TestCase {

	@Autowired
	private AdvertRepository advertRepro;

	@Autowired
	private UserRepository userRepro;

	@Before
	public void setup() {
		BayUser user = new BayUser();
		user.setFirstName("Testi");

		Advert one = new Advert();
		one.setTitle("I'm number one");

		Advert two = new Advert();
		two.setTitle("I'm number two");

		one.setUser(user);
		two.setUser(user);

		user.setAdverts(Arrays.asList(one, two));

		userRepro.saveAndFlush(user);
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void testInsert_noUser() {
		Advert standalone = new Advert();
		standalone.setDescription("A test advert");

		advertRepro.saveAndFlush(standalone);
	}

	@Test(expected = InvalidDataAccessApiUsageException.class)
	public void testInsert_userNotSaved() {
		Advert advert = new Advert();
		advert.setDescription("A test advert");
		BayUser advertUser = new BayUser();
		advertUser.setFirstName("AdvertUser");
		advert.setUser(advertUser);

		advertRepro.saveAndFlush(advert);
	}

	@Test
	public void testInsert_UserAlreadyPersited() {
		BayUser user = new BayUser();
		user.setFirstName("advertUser");

		BayUser dbUser = userRepro.saveAndFlush(user);

		Advert advert = new Advert();
		advert.setDescription("A test advert");
		advert.setUser(dbUser);

		advertRepro.saveAndFlush(advert);
		Advert dbAdvert = advertRepro.findByDescription("A test advert");

		assertEquals("A test advert", dbAdvert.getDescription());
		assertEquals("advertUser", dbAdvert.getUser().getFirstName());
	}

	@Test
	public void testRetrieve_ByUserId() {
		BayUser user = new BayUser();
		user.setFirstName("Testi");
		
		BayUser dbUser = userRepro.saveAndFlush(user);

		Advert advertOne = new Advert();
		Advert advertTwo = new Advert();

		advertOne.setDescription("One");
		advertTwo.setDescription("Two");

		
		advertOne.setUser(dbUser);
		advertTwo.setUser(dbUser);

		advertRepro.saveAndFlush(advertOne);
		advertRepro.saveAndFlush(advertTwo);

		List<Advert> dbAdverts = advertRepro.findByUserId(dbUser.getId());

		assertNotNull(dbAdverts);
		assertEquals(2, dbAdverts.size());
	}

	@Test
	public void testRetrieve_ByCountryAndState() {
		BayUser user = new BayUser();
		user.setFirstName("Testi");

		BayUser dbUser = userRepro.saveAndFlush(user);

		Advert advert = new Advert();

		advert.setCountry(Country.IRL);
		advert.setState(AdvertState.OPEN);
		advert.setUser(dbUser);

		advertRepro.saveAndFlush(advert);

		List<Advert> dbAdverts = advertRepro.findByCountryAndState(Country.IRL, AdvertState.OPEN);

		assertNotNull(dbAdverts);
		assertEquals(1, dbAdverts.size());
	}

	@Test
	public void testRetrieve_ByRangeOfIds() {
		List<Advert> list = advertRepro.findByRangeOfIds(Arrays.asList(1L, 2L));
		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals(true, list.stream().filter(a -> a.getTitle().equals("I'm number one")).findAny().isPresent());
	}

}
