package io.dublinbrains.videobay;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;

import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.advert.AdvertRepository;
import io.dublinbrains.videobay.model.conversation.Conversation;
import io.dublinbrains.videobay.model.conversation.ConversationRepository;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.model.user.UserRepository;
import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { VideoBayAppTest.class })
@DataJpaTest
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class ConversationRepositoryTest extends TestCase {
	
	@Autowired
	private AdvertRepository advertRepo;

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ConversationRepository conversationRepo;

	@Before
	public void setup() {
		BayUser user = new BayUser();
		user.setFirstName("Testi");

		Advert one = new Advert();
		one.setTitle("I'm number one");

		one.setUser(user);

		user.setAdverts(Arrays.asList(one));

		userRepo.saveAndFlush(user);
		
		Advert dbAd = advertRepo.findAll().get(0);
		Conversation newConv = new Conversation();
		newConv.setAdvert(dbAd);
		newConv.setQuestion("A question?");
		newConv.setAnswer("Master is here");
		newConv.setQuestionerId(155L);
		
		conversationRepo.saveAndFlush(newConv);
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void testInsert_noAdvert() {
		Conversation conv = new Conversation();
		conversationRepo.saveAndFlush(conv);
	}
	
	@Test
	public void testInsert(){
		Advert dbAd = advertRepo.findAll().get(0);
		Conversation newConv = new Conversation();
		newConv.setAdvert(dbAd);
		newConv.setQuestion("NEW");
		newConv.setAnswer("YES");
		newConv.setQuestionerId(155L);
		
		conversationRepo.saveAndFlush(newConv);
		
		List<Conversation> dbConvs = conversationRepo.findAll();
		assertEquals(2, dbConvs.size());
	}
	
	@Test
	public void testFindByAdvertId(){
		List<Conversation> convs = conversationRepo.findByAdvertId(1L);
		assertEquals(1, convs.size());
	}
	
	@Test
	public void updateById(){
		Conversation dbConv = conversationRepo.findByAdvertId(1L).get(0);
		dbConv.setQuestion("UPDATE");
		
		conversationRepo.saveAndFlush(dbConv);
		
		Conversation updatedConv = conversationRepo.getOne(1L);
		assertEquals("UPDATE", updatedConv.getQuestion());
	}
}
