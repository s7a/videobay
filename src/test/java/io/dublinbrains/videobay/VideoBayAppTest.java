package io.dublinbrains.videobay;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "io.dublinbrains.videobay.model" })
@EntityScan(basePackages = { "io.dublinbrains.videobay.model" })
public class VideoBayAppTest {

}
