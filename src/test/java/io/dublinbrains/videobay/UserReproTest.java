package io.dublinbrains.videobay;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import io.dublinbrains.videobay.model.account.Account;
import io.dublinbrains.videobay.model.account.AccountRepository;
import io.dublinbrains.videobay.model.account.Address;
import io.dublinbrains.videobay.model.advert.Advert;
import io.dublinbrains.videobay.model.user.BayUser;
import io.dublinbrains.videobay.model.user.UserRepository;
import junit.framework.TestCase;

/**
 * Unit test for VideoBayAppTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { VideoBayAppTest.class })
@DataJpaTest
public class UserReproTest 
		extends TestCase
{

	@Autowired
	private UserRepository userRepro;
	
	@Autowired
	private AccountRepository accountRepro;

	@Test
	public void testInsertAndRetrieveUser()
    {
		BayUser testUser = new BayUser();
		testUser.setFirstName("Arturo");
		testUser.setSurName("Tester");
		testUser.setPassword("1234");
		testUser.setEmail("test@testi.com");
		testUser.setScreenName("Artii");

		userRepro.saveAndFlush(testUser);

		BayUser dbUser = userRepro.findByScreenName("Artii");

		assertEquals(testUser.getFirstName(), dbUser.getFirstName());

    }

	@Test
	public void testInsertWithAdvert() {
		BayUser testUser = new BayUser();
		testUser.setScreenName("test");

		Advert testAdvert = new Advert();
		testAdvert.setDescription("test");

		testUser.setAdverts(Arrays.asList(testAdvert));
		testAdvert.setUser(testUser);

		userRepro.saveAndFlush(testUser);
		
		BayUser dbUser = userRepro.findByScreenName("test");

		assertNotNull(dbUser);
		assertEquals("test", dbUser.getScreenName());
		assertEquals(1, dbUser.getAdverts().size());
		assertEquals("test", dbUser.getAdverts().get(0).getDescription());

	}

	@Test
	public void testUserNotPresent() {
		BayUser dbUser = userRepro.findById(666L);

		assertNull(dbUser);
	}

	@Test
	public void testAddFaforiteAdvert() {
		BayUser user = new BayUser();
		user.setFirstName("Test");
		user.setFavoriteAds(new HashSet<>(Arrays.asList(1L)));

		BayUser dbUser = userRepro.saveAndFlush(user);

		dbUser = userRepro.findOne(dbUser.getId());

		assertNotNull(dbUser.getFavoriteAds());
		assertEquals(1L, dbUser.getFavoriteAds().stream().findFirst().get().longValue());
	}

	@Test
	public void testDeleteFavorite() {
		BayUser user = new BayUser();
		user.setFirstName("Test");
		user.setFavoriteAds(new HashSet<>(Arrays.asList(1L)));

		BayUser dbUser = userRepro.saveAndFlush(user);

		dbUser = userRepro.findOne(dbUser.getId());

		dbUser.getFavoriteAds().remove(1L);
		userRepro.saveAndFlush(dbUser);

		dbUser = userRepro.findOne(dbUser.getId());

		assertEquals(dbUser.getFavoriteAds().size(), 0);
	}
	
	@Test
	public void testAddAccount(){
		BayUser user = new BayUser();
		user.setFirstName("test");
		
		BayUser dbUser = userRepro.saveAndFlush(user);
		
		assertNull(dbUser.getAccount());
		
		Account account = new Account();
		account.setUser(dbUser);
		
		Address addOne = new Address();
		Address addTwo = new Address();
		
		addOne.setCity("Dublin");
		addTwo.setCity("Trier");
		addOne.setAccount(account);
		addTwo.setAccount(account);
		
		account.setAddresses(Arrays.asList(addOne,addTwo));
		
		dbUser.setAccount(account);
		
		userRepro.saveAndFlush(dbUser);
		
		List<BayUser> accountUsers = userRepro.findAll();
		
		assertNotNull(accountUsers);
		assertEquals(accountUsers.size(), 1);
		assertNotNull(accountUsers.get(0).getAccount());
		assertNotNull(accountUsers.get(0).getId());
		assertEquals(accountUsers.get(0).getAccount().getAddresses().size(), 2);
		assertTrue(accountUsers.get(0).getAccount().getAddresses().stream().filter(a -> a.getCity().equals("Trier")).findAny().isPresent());
		assertNotNull(accountUsers.get(0).getAccount().getAddresses().stream().filter(a -> a.getCity().equals("Trier")).findAny().get().getId());
		assertTrue(accountUsers.get(0).getAccount().getAddresses().stream().filter(a -> a.getCity().equals("Dublin")).findAny().isPresent());
		assertNotNull(accountUsers.get(0).getAccount().getAddresses().stream().filter(a -> a.getCity().equals("Dublin")).findAny().get().getId());
		
		Account dbAccount = accountRepro.findOne(1L);
		assertNotNull(dbAccount);
	}
}
