package io.dublinbrains.videobay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.file.Path;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import io.dublinbrains.videobay.service.file.FIleUploadService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { VideoBayAppTest.class })
@ActiveProfiles("test")
public class FileUploadTest
{

	@Autowired
	FIleUploadService fileService;

	@Test
	public void testConstructNginxFileUrl()
	{
		final String nginxServer = (String) ReflectionTestUtils.getField(fileService, "nginxServerAddress");
		final Path basePath = (Path) ReflectionTestUtils.getField(fileService, "basePath");
		assertNotNull(basePath);
		assertNotNull(nginxServer);
		final StringBuilder absolutePathStrBuilder = new StringBuilder(basePath.toString());
		absolutePathStrBuilder.append("/1/SuperVideo.mp4");
		final String result = (String) ReflectionTestUtils.invokeMethod(fileService, "constructNginxFileUrl",
				absolutePathStrBuilder.toString(), nginxServer);
		assertEquals(nginxServer + "/content/1/SuperVideo.mp4", result);
	}

}
