package io.dublinbrains.videobay;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import io.dublinbrains.videobay.service.video.ffmpeg.FfmpegPostProcessingService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { VideoBayAppTest.class })
@ActiveProfiles("test")
public class FfmpegPostProcessingServiceTest {
	
	@Autowired
	private FfmpegPostProcessingService ffmpegPostProcessingService;
	
	@Mock
	private Path filePatch;
	
	@Mock
	private Path fileNamePath;
	
	@Before
	public void setUp(){
		
		Mockito.when(filePatch.getFileName()).thenReturn(fileNamePath);
		Mockito.when(fileNamePath.toString()).thenReturn("VID_20181017_192744.mp4");
	}
	
	@Test
	public void testConstructConvertedFileName(){
		final String result = (String) ReflectionTestUtils.invokeMethod(ffmpegPostProcessingService, "constructConvertedFileName",
				filePatch, "_small1.mp4");
		assertEquals("VID_20181017_192744_small1.mp4", result);
	}
}
